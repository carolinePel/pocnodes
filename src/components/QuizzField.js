import React, { Component } from 'react';

export default class QuizzField extends Component{
    
    chooseColor(){

        let color = ""
        let wrongAndChecked = this.props.hasSubmitHappened 
                            && this.props.hasBeenChecked 
                            && !this.props.isTrue
        let trueAndNotChecked = this.props.hasSubmitHappened
                                && !this.props.hasBeenChecked
                                && this.props.isTrue
        let wrong = this.props.hasSubmitHappened
                    && !this.props.hasBeenChecked 
                    && !this.props.isTrue
        
        if(wrongAndChecked){
            color = 'red'
        }else if(wrong){
            color = 'grey'
        }else if(trueAndNotChecked){
            color = 'LightGreen'
        }else{
            color = 'green'
        } 

        return color
    }

    render(){
        
        const color = this.chooseColor();
        return (
            <li  className = "noBullet" style = {{background:color}}>
                <label className = "quizzListLink">
                    <input   
                        type = {this.props.type} 
                        name = {this.props.content} 
                        checked = {this.props.hasBeenChecked} 
                        onChange = {this.props.onChange} 
                    />
                    {this.props.content}
                </label>
            </li>
        );
    }
}
