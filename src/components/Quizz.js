import React, { Component } from 'react';
import QuizzField from './QuizzField';

/**
 * Multichoice form
 */

export default class Quizz extends Component{
  constructor(props){
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.state = {
      checkedItems: new Map(),
      hasSubmitHappened: false,
      isSubmitButtonHidden: false,
    }
  }
    
  handleClick(e){
    e.preventDefault()
    
    const { checkedItems } = this.state
    
    //get user's answers
    let userResults = [];
    checkedItems.forEach((value,key) => {
      userResults.push(key);
    })
    //send them to parent component
    this.props.updateUserResults(userResults);
    //move to next quizz
    if(this.props.questionId < this.props.questionTotal)
      this.props.updateQuestionId();

    this.setState({
      hasSubmitHappened: true,
      isSubmitButtonHidden: true,
    });
    setTimeout(()=>{
      this.props.triggerPlay()
    }, 1000);
  }

  handleInputChange(e){

    const item = e.target.name;
    const isChecked = e.target.checked;

    this.setState( prevState => ({ checkedItems: prevState.checkedItems.set(item, isChecked) }));
  }

  render(){
    //console.log(this.props);
      return(
        <div className = "quizContainer">
          <p className = "ta-l white">Question {this.props.quizzNb + 1} sur {this.props.questionTotal}</p>
          <p className = "ta-l white">{this.props.question}</p>

          <form>
            <ul className = "quizListContainer">
              {this.props.content.answers.map((object,index) => {
                //console.log(object)
                return  (
                        <QuizzField 
                            key = {"QuizzFieldkey" + index} 
                            type = "checkbox"
                            hasBeenChecked = {this.state.checkedItems.has(object.option)}
                            hasSubmitHappened = {this.state.hasSubmitHappened}
                            isTrue = {object.isCorrect}
                            content = {object.option}
                            onChange = {this.handleInputChange}
    
                        />
                      );
              })}
            </ul>
            {!this.state.isSubmitButtonHidden && 
              <input 
                  style = {{position:"absolute"}} 
                  onClick = {this.handleClick} 
                  type = "submit" 
                  value = "ok"
              />
            }
          </form>
        </div>
      );
    }
  }
  