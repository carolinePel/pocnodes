import './App.css';
import { Player, ControlBar, PlayToggle} from 'video-react';
import React, { Component } from 'react';
import "video-react/dist/video-react.css";
import Quizz from './components/Quizz';


/**
 * App container
 */

class InteractiveVideo extends Component {
  constructor(props){

    super(props)

    //this.togglePlay = this.togglePlay.bind(this)
    this.handleUserResults = this.handleUserResults.bind(this)
    this.handleNodeId = this.handleNodeId.bind(this)


    this.state = {
      score: 0,
      currentNodeId:'node_1',
      comingNode:'node_2',
      totalQuizzes: Object.keys(this.props.nodes).length,
      currentSrc: '',
      isPlaying: false,
      quizzesLength: 0,
      isQuizzHidden: true,
      userResult: [],
      playedNodesNb:0
    }
  }

  componentDidMount() {

    //this.loadNextQuizz()
    // subscribe state change
    this.refs.player.subscribeToStateChange(this.handleStateChange.bind(this))
   }

   /**
    * 
    * @param {Object} state 
    * @param {Object} prevState 
    */

  handleStateChange(state, prevState) {

    // Get player state to this component's state
    this.setState({
      player: state,
    })

    const  currentNode  = this.props.nodes[this.state.currentNodeId]
    const currentQuizz = currentNode.quizz

    //Make quizz appear and disappear
    if(currentNode !== undefined){

        const answerNumber = this.state.userResult.length
        //console.log('answerNumber',answerNumber)
        const isPlayingAgain = !state.paused && answerNumber > 0
        const shouldShowQuiz = 
            state.currentTime > currentQuizz.timeCode
            && !state.paused 
            && answerNumber === 0
        const endOfCurrentNode = Math.trunc(state.currentTime) === currentNode.end
        //console.log(state.currentTime)
        if(shouldShowQuiz)
        {
            this.pause()
            this.quizzAppear()
            
        }
        //when the Player has been restarted after question answer
        if(isPlayingAgain)
        {
            this.quizzDisappear()
            this.getNextNode()
            //setTimeout(this.loadNextNodes.bind(this), 500)
        }
        if(endOfCurrentNode)
        {
          
          this.changeNode()
          // this.playCurrentNode()
          // setTimeout(
          // this.playCurrentNode,500)
        }
    }
  }

  /**
   * QuizzAppearance
   */

  quizzAppear () {
    this.setState({
        isQuizzHidden: false
      }) 
  }

  quizzDisappear () {

    this.setState({
      isQuizzHidden: true
    })

  }


  pause = () => {

    this.setState({
      isPlaying: !this.state.isPlaying
    })
    this.refs.player.pause();

  }

  // togglePlay(e){

  //   e.preventDefault()

  //   this.setState({
  //     isPlaying: !this.state.isPlaying
  //   })

  //   if(!this.state.isPlaying)
  //     this.refs.player.play()
  //   else
  //   {
  //     this.refs.player.pause()
  //   }

  // }

  playCurrentNode = () => {

    const currentNode  = this.props.nodes[this.state.currentNodeId]
    console.log(currentNode)
    const timeCodeStart = currentNode.start
    //console.log(timeCodeStart)
    this.refs.player.seek(timeCodeStart)
    //this.refs.player.load()
    this.refs.player.play()

  }

  seek =  (e) => {
    e.preventDefault()
    this.playCurrentNode()
  }


  load() {
    this.refs.player.load();
  }

  changeNode = () => {

    this.setState({
      currentNodeId:this.state.comingNode,
      userResult:[],
      playedNodesNb:this.state.playedNodesNb + 1
    }, this.playCurrentNode )

  }

  getNextNode = () => {

    const node = this.props.nodes[this.state.currentNodeId]

    const userIsRight = this.isUserRight(node.quizz.answers,this.state.userResult)

    const nextNode = (userIsRight)? node.nodeIsCorrect : node.nodeIsWrong;

    this.setState({
      comingNode: nextNode
    })
  }

  handleTriggerPlay = () => {
    console.log('triggerPlay!!!')
    this.refs.player.play()
  }

  isUserRight = (answersOption,responses) => {

    let isTrue = true
    //in all options
    answersOption.forEach((optionObj,index) => {
      const isCheckedAndWrong = responses.includes(optionObj.option) 
                                && !optionObj.isCorrect
      const isNotCheckedAndTrue = !responses.includes(optionObj.option)
                                  && optionObj.isCorrect
      
      if(isCheckedAndWrong)
      {
        //user is wrong
        isTrue = false
      }
      if(isNotCheckedAndTrue){
        isTrue = false
      }

    })

    return isTrue
  } 
/**
 * careful before deleting because in the quizz
 */

  handleNodeId(){

  }
/**
 * arg = array
 */

  handleUserResults(result){
    
    this.setState({
        userResult: result
    })
  }

  render() {
  
    const { nodes } =  this.props 
    const  currentNode  = this.props.nodes[this.state.currentNodeId]

    //console.log(currentNode.src)
    const comingNodeTrue = currentNode.nodeIsCorrect;
    const comingNodeFalse = currentNode.nodeIsWrong;
    //console.log(comingNodeFalse);
    //console.log(currentNode.quizz)
    //console.log(this.state.comingNode)
    return (
      <div className = "App">
        <div className = "poc">
          <Player 
                ref   = 'player'
                fluid = {false}
                width = {this.props.width}
                height = {this.props.height}>
            <source src = {currentNode.src}/>
            <ControlBar autoHide = {false} disableDefaultControls = {false}>
                <PlayToggle />
            </ControlBar>
          </Player>
          <div className = "col">
          {!this.state.isQuizzHidden && 
            <Quizz
                content = {currentNode.quizz}
                questionTotal = {this.state.totalQuizzes}
                updateUserResults = {this.handleUserResults}
                updateNodeId = {this.handleNodeId}
                triggerPlay = {this.handleTriggerPlay}
                quizzNb = {this.state.playedNodesNb}
            />}
          </div>
        </div>
        <div>
          <a href = "" 
            onClick = {this.seek} 
            className = "btn green">Seek</a>
        </div>
      </div>
    );
  }
}


export default InteractiveVideo
