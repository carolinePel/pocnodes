 const nodes = {
        "node_1":{
            src:'https://media.w3.org/2010/05/sintel/trailer_hd.mp4',
            start:4,
            end:10,
            nodeIsCorrect:'node_2',
            nodeIsWrong:'node_3',
            defaultFollowing:'node_2',
            quizz:{
                "question": "Quelle est votre première réaction devant un client?",
                "timeCode":8,
                "type":'multichoice',
                "answers": [
                    {
                        option:"Je pars en courant",
                        isCorrect:false,
                    },
                    {
                        option:"Je lui dis bonjour et je le laisse tranquille",
                        isCorrect:false,
                    },
                    {
                        option:"Je vais le saluer et lui demander ce dont il a besoin",
                        isCorrect:true,
                    },
                    {
                        option:"Je téléphone à mon boss pour savoir quoi faire",
                        isCorrect:true,
                    }, 
                ]
            }
        },
        "node_2":{
            src:'http://media.w3.org/2010/05/bunny/trailer.mp4',
            start:40,
            end:50,
            nodeIsCorrect:'node_1',
            nodeIsWrong:'node_3',
            defaultFollowing:'node_3',
            quizz:{
                "question": "Les clients vous tapent-ils sur les nerf?",
                "timeCode":45,
                "type":'multichoice',
                "answers": [
                    {
                        option:"Oui, souvent",
                        isCorrect:false,
                    },
                    {
                        option:"quelques fois",
                        isCorrect:false,
                    },
                    {
                        option:"oui, mais je suis quelqu'un de poli",
                        isCorrect:false,
                    },
                    {
                        option:"Jamais, j'adore mes clients!",
                        isCorrect:true,
                    }, 
                ],
            }
        },
        "node_3":{
            src:'http://media.w3.org/2010/05/bunny/movie.mp4',
            start:20,
            end:30,
            nodeIsCorrect:'node_2',
            nodeIsWrong:'node_1',
            defaultFollowing:'node_1',
            quizz:{
                "question": "Quand un client s'en va, quelle est votre réaction?",
                "timeCode":25,
                "type":'multichoice',
                "answers": [
                    {
                        option:"Bon débarras!",
                        isTrue:false,
                    },
                    {
                        option:"Je le salue, et lui dis de revenir bientôt",
                        isTrue:false,
                    },
                    {
                        option:"Je peux retourner lire l'équipe sur mon téléphone",
                        isTrue:false,
                    },
                    {
                        option:"Je me dirige vers un autre client après l'avoir salué",
                        isTrue:true,
                    },
                ]
            }
        }
    }
        
export default nodes;