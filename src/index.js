import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import quizzContent from './api/quizzContent';
import InteractiveVideo from './InteractiveVideo';

const videoWidth = 650
const videoHeigth = 400

ReactDOM.render(<InteractiveVideo 
    nodes={quizzContent} 
    width={videoWidth}
    height={videoHeigth}
/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
